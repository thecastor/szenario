# Szenario (Scenario Builder)
## Applying the Gradle Plugin
To apply the plugin, first copy the plugin jar file to your current project, e.g., to the `libs` folder.

The following snippet shows how to apply the plugin from a (local) jar file

```
buildscript {
    repositories {
        // in this example, the plugin is located at ./libs/szenario-0.1.jar
        flatDir { dirs("libs") }
    }
    dependencies {
        classpath("de.thecastor.test:szenario:0.1")
    }
}
plugins {
    kotlin("jvm") version "1.4.21"
}
apply(plugin = "de.thecastor.szenario")
```
### Plugin DSL
Applying the plugin locally using the Plugin DSL isn't supported, 
`de.thecastor.szenario:de.thecastor.szenario.gradle.plugin:0.1` 
because plugin resolution will look for the plugin artifact 
(`plugin-id:plugin-id.gradle.plugin:version`), rather than `de.thecastor.test:szenario:0.1`. 
```
repositories {
    flatDir { dirs("libs") }
}
plugins {
    kotlin("jvm") version "1.4.21"
    id("de.thecastor.szenario")
}
```
## Defining a Szenario DSL
In order to create and run your very own Szenarios, you will first define a custom Szenario DSL.

The Szenario Plugin will translate your definition to actual DSL classes. 
The DSL can then be used to create Szenarios.

The following explains the general process:
1. Defining a DSL  
    ```kotlin
   // 'Car having Brakes'
   Car::class relatesTo Brake::class using { car ->
     // TODO: provide Brakes to your Car instance (single, list, array)
   } designated "having"
   ```
2. Szenario Plugin will translate the DSL definition to actual class sources whenever you run either any `test` task, or `szenarioGenerate` explicitly.  
The resulting classes provide the tools to create Szenarios, specifically for your domain.
3. Create Szenarios  
   In order to create Szenarios using the newly created DSL classes, these classes need to be added to the test source sets.  
   Because they are based on Szenario classes, these have to be added as dependencies to the test implementation.
   ```
   // define DSL source and target directories
   szenario {
     definitionDir = "src/main/szenario" // DSL definition
     targetDir = "src/test/szenario"     // DSL class sources
   }
   // make DSL classes available in tests
   sourceSets.test {
     java.srcDirs("src/test/szenario")
   }
   // provide Szenario base classes in tests
   dependencies {
     testImplementation("de.thecastor.test:szenario")
   }
   ```
   Now that the Szenario DSL is available in tests, you're good to create your first Szenarios.
    ```kotlin
   // !!! WIP !!!
   
   // Setup phase
   Car().having(Brakes)
   // Test phase
   .should().be().able().to(::canBrake)
   
   Car().should().not().be().able().to(::canBrake)
   
   // Setup phase
   Car().having(Brakes)
   // Operation phase
   .brake() // method provided by Car
   // Test phase
   .speed  // property provided by Car
   .should().equal(0) // .be(eq(0))
   .brakeTemperature.should().be(gte(10.0))
   .brakeTemperature.should { it.toInt() > 0 }
   
   // Setup phase
   Car()
   // Operation phase
   .that(DealerShip::sell)  // method operating on Car, provided by context
   // Test phase
   .licensePlate.should().exist() // .not().be(null()), not().be(eq(null))
   ```
