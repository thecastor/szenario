import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert

fun assertContains(str: String, subString: String?,
                   reason: String = "String should contain substring",
                   ignoreCase: Boolean = true) {
    MatcherAssert.assertThat(
        reason,
        str,
        if (ignoreCase) CoreMatchers.containsStringIgnoringCase(subString)
        else CoreMatchers.containsString(subString)
    )
}