import de.thecastor.test.szenario.gradle.GenerateSzenarioTask
import mu.KLogging
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.nio.file.Path
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class CodeLinesPluginTest {

    @TempDir
    lateinit var testProjectDir: Path

    private lateinit var buildFile: File
    private lateinit var gradleRunner: GradleRunner

    @BeforeEach
    fun setup() {

        buildFile = testProjectDir.resolve("build.gradle.kts").toFile()
                .apply {
                    appendText("""
                        buildscript {
                            repositories {
                                flatDir { dirs("libs") }
                            }
                            dependencies {
                                classpath("de.thecastor.test:szenario:0.1")
                            }
                        }
                        plugins {
                            kotlin("jvm") version "1.4.21"
                            id("de.thecastor.szenario") version "0.1"
                        }
                        repositories {
                            mavenCentral()
                        }
                        """.trimIndent())
                }
        testProjectDir.resolve("settings.gradle.kts").toFile()
                .appendText("""
                        rootProject.name = "szenario-plugin-test"
                    """.trimIndent())

        File("build/libs").copyRecursively(
                testProjectDir.resolve("libs").toFile().apply { mkdir() }
        )

        gradleRunner = GradleRunner.create()
                .withPluginClasspath()
                .withProjectDir(testProjectDir.toFile())
                .withTestKitDir(testProjectDir.resolve(UUID.randomUUID().toString()).toFile())
    }

    @Test
    fun `'assemble' should execute successfully (basic setup)`() {
        val result = assertDoesNotThrow {
            gradleRunner
                    .withArguments("assemble")
                    .build()
        }

        assertContains(result.output, "BUILD SUCCESSFUL",
                       "Output should indicate successful build")
    }

    @Test
    fun `'szenarioGenerate' should execute successfully`() {
        val result = assertDoesNotThrow {
            gradleRunner
                    .withArguments(GenerateSzenarioTask.NAME)
                    .build()
        }

        val task = assertNotNull(result.task(":${GenerateSzenarioTask.NAME}"),
                                 "Task should exist: ${GenerateSzenarioTask.NAME}")
        assertEquals(TaskOutcome.SUCCESS, task.outcome,
                     "Task should execute successfully")
        assertContains(result.output, "BUILD SUCCESSFUL",
                       "Output should indicate successful build")
    }

    @Test
    fun `Task execution without changes should indicate 'Up-To-Date'`() {
        gradleRunner
                .withArguments(GenerateSzenarioTask.NAME)
                .build()

        val result = assertDoesNotThrow {
            gradleRunner
                    .withArguments(GenerateSzenarioTask.NAME)
                    .build()
        }

        val task = assertNotNull(result.task(":${GenerateSzenarioTask.NAME}"),
                                 "Task should exist: ${GenerateSzenarioTask.NAME}")
        assertEquals(TaskOutcome.UP_TO_DATE, task.outcome,
                     "Task should be up-to-date")
        assertContains(result.output, "BUILD SUCCESSFUL",
                       "Output should indicate successful build")
    }

    companion object : KLogging()

}