package de.thecastor.test.szenario.base.spec

import kotlin.reflect.KClass

/**
 * This is the base class of all registered domain Entities.
 */
sealed class DomainEntity<TActual : Any>(
    val actual: KClass<TActual>
) : Entity<TActual>
