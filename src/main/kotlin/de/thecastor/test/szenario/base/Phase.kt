package de.thecastor.test.szenario.base

interface Phase {
    val name: String
}