package de.thecastor.test.szenario.base

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

interface EntityFactory {
    /**
     * Creates Entities of types specified as being part of the domain.
     */
    fun <T : Any> create(type: KClass<out T>, context: Context): T
}
