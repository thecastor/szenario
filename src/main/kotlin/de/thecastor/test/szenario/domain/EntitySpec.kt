package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

class EntitySpec<T : Any, TContext : Context>(
    val context: TContext,
    val type: KClass<T>,
    var name: String = type.simpleName ?: throw IllegalArgumentException("illegal (anonymous) type"),
    var using: TContext.() -> T = {
        entityFactory.create(type, this)
    }
) {

    private val _relationBuilders = mutableListOf<RelationSpec.Builder<TContext, T, *>>()
    private val _relations = mutableMapOf<String, RelationSpec<TContext, T, *>>()
    val relations: Map<String, RelationSpec<TContext, T, *>>
        get() = _relations.toMap()

    private fun <To : Any> onBuild(): (RelationSpec.Builder<TContext, T, To>, RelationSpec<TContext, T, To>) -> Unit =
        { builder, spec ->
            addRelation(spec)
            _relationBuilders.remove(builder)
        }

    private fun <To : Any> addRelation(spec: RelationSpec<TContext, T, To>) {
        _relations.putIfAbsent(spec.identifier, spec)?.also {
            throw EntitySpecException(
                "Duplicate relation specified: %s --(%s)--> %s".format(
                    this@EntitySpec.name,
                    spec.via,
                    spec.to.name
                )
            )
        }
    }

    fun <To : Any> relatesTo(
        entity: EntitySpec<To, TContext>,
        using: TContext.() -> Unit = {}
    ): RelationSpec.Builder<TContext, T, To> =
        RelationSpec.Builder(
            RelationSpec.RelationSpecBuilderContext(context, this),
            SimpleProvider(entity),
            using,
            onBuild()
        ).also { builder -> _relationBuilders.add(builder) }

    fun <To : Any> relatesTo(
        entity: EntitySpecProvider<To, TContext>,
        using: TContext.() -> Unit = {}
    ): RelationSpec.Builder<TContext, T, To> =
        RelationSpec.Builder(RelationSpec.RelationSpecBuilderContext(context, this), entity, using, onBuild())
            .also { builder -> _relationBuilders.add(builder) }

    /**
     * A unique identifier for [RelationSpec]s to ensure properties are only referenced once.
     */
    private val RelationSpec<*, *, *>.identifier
        get() = this.via

    internal fun checkState() {
        check(_relationBuilders.isEmpty()) {
            StringBuilder("EntitySpec has unfinished relations: ").apply {
                _relationBuilders.map { appendLine().append("- ").append(it) }
            }.toString()
        }
    }
}
