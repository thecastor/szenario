package de.thecastor.test.szenario.domain

interface Domain {
    val entities: List<EntitySpec<*, *>>
}

@DslMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
annotation class Specification
