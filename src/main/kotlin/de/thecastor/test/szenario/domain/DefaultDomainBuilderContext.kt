package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.base.EntityFactory
import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

class DefaultDomainBuilderContext private constructor() : DomainBuilderContext {

    private val entitySpecs = mutableListOf<EntitySpec<*, Context>>()
    private val objects = mutableSetOf<DomainObject>()
    private val aliases = mutableMapOf<String, Alias>()
    private val relations = mutableListOf<Relation<*, *>>()

    override val parent: Context? = null
    override val entityFactory: EntityFactory
        get() = TODO("Not yet implemented")

    override val entitySpecResolver: EntitySpecResolver = object : EntitySpecResolver {
        override fun <T : Any> resolve(name: String, type: KClass<T>): EntitySpecProvider<T, Context> =
            SimpleProvider(name, type) {
                entitySpecs.find { it.name == name }?.let { it ->
                    if (it.type != type) {
                        throw DomainSetupException(
                            "Type mismatch. Resolved EntitySpec['%s'] has type %s, expected %s".format(
                                it.name,
                                it.type,
                                type
                            )
                        )
                    }
                    @Suppress("UNCHECKED_CAST")
                    it as EntitySpec<T, Context>
                } ?: throw DomainSetupException("No EntitySpec was specified with name '$name'")
            }
    }

    override fun register(entity: EntitySpec<*, Context>) {
        entitySpecs.add(entity)
    }

    override fun register(relation: Relation<*, *>) {
        relations.add(relation)
    }

    override fun alias(obj: DomainObject, alias: String) {
        objects.find { it == obj }?.also {
            aliases.putIfAbsent(alias, Alias(obj, alias))?.also {
                throw IllegalArgumentException("alias already registered: $alias -> ${it::class.qualifiedName}")
            }
        } ?: throw IllegalArgumentException("object of type ${obj::class.qualifiedName} not found")
    }

    override fun build(): Domain {
        entitySpecs.forEach { it.checkState() }

        return object : Domain {
            override val entities: List<EntitySpec<*, *>> = entitySpecs
        }
    }

    companion object {
        internal fun init() {
            DomainBuilderContextProxy.init(DefaultDomainBuilderContext())
        }
    }
}
