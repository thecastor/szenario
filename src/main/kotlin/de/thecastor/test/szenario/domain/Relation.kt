package de.thecastor.test.szenario.domain

import kotlin.reflect.KClass

interface Relation<From : Any, To : Any> : DomainObject {
    val from: KClass<From>
    val to: KClass<To>
    val designation: String
    val alias: String?

    fun alias(alias: String)
}

private class RelationImpl<From : Any, To : Any>(
    override val from: KClass<From>,
    override val to: KClass<To>,
    override val designation: String
) : Relation<From, To> {

    override val alias: String? get() = _alias
    private var _alias: String? = null

    init {
        DomainBuilderContextProxy.register(this)
    }

    override fun alias(alias: String) {
        if (_alias != null) throw IllegalStateException("alias already defined: $_alias")
        else _alias = alias
    }
}

infix fun <From : Any, To : Any> KClass<From>.relatesTo(to: KClass<To>): Relation<From, To> =
    RelationImpl(this, to, "relatesTo")

//infix fun <From : Any, To : Any> KClass<From>.relatesTo(to: DomainObjectAlias): Relation<From, To> =
//    RelationImpl(this, to, "relatesTo")

infix fun <From : Any, To : Any> Relation<From, To>.designated(designation: String): Relation<From, To> {
    require(designation.isNotBlank()) {
        "designation must not be blank"
    }
    return RelationImpl(from, to, designation)
}

infix fun <T : DomainObject> T.alias(alias: String): T = apply {
    require(alias.isNotBlank()) {
        "designation must not be blank"
    }
    DomainBuilderContextProxy.alias(this, alias)
}

fun bleh() {
    String::class relatesTo CharArray::class designated "extends" alias "bleh"
    X relatesTo X
    X relatesTo CharArray::class
//    X relatesTo CharArray::class designated "blubb"
}

interface Relatable<Self : Any> {
    @Suppress("UNCHECKED_CAST")
    infix fun <To : Any> relatesTo(to: KClass<To>): Relation<Self, To> =
        ((if (this::class.isCompanion) this::class.java.declaringClass.kotlin
        else this::class) as KClass<Self>).relatesTo(to)

    @Suppress("UNCHECKED_CAST")
    infix fun <To : Any> relatesTo(to: Relatable<To>): Relation<Self, To> =
        relatesTo(
            (if (to::class.isCompanion) this::class.java.declaringClass.kotlin
            else to::class) as KClass<To>
        )
}

class X {

    companion object : Relatable<X>
}

