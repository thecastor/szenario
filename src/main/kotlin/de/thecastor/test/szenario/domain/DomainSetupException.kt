package de.thecastor.test.szenario.domain

class DomainSetupException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)
