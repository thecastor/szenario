package de.thecastor.test.szenario.domain

data class Alias(val obj: DomainObject, val alias: String)
