package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context

interface DomainBuilderContext : Context {
    val entitySpecResolver: EntitySpecResolver

    fun register(entity: EntitySpec<*, Context>)

    fun register(relation: Relation<*, *>)

    fun alias(obj: DomainObject, alias: String)

    fun build(): Domain
}
