package de.thecastor.test.szenario.domain

class EntitySpecException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)
