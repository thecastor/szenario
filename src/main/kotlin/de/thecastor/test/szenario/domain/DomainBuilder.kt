package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

interface DomainBuilder {
    val entitySpecResolver: EntitySpecResolver

    fun <TResult : Any> Entity(
        type: KClass<TResult>,
        setUp: EntitySpec<TResult, Context>.() -> Unit = {}
    ): EntitySpec<TResult, Context>
}

@Specification
@Suppress("FunctionName")
fun Szenario(specs: DomainBuilder.() -> Unit) {
    DefaultDomainBuilder().specs()
}
