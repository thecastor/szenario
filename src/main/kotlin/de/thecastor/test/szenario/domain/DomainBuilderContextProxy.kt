package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.base.EntityFactory
import de.thecastor.test.szenario.common.Context

object DomainBuilderContextProxy : DomainBuilderContext {

    override val parent: Context?
        get() {
            checkInitialized()
            return delegate.parent
        }
    override val entityFactory: EntityFactory
        get() {
            checkInitialized()
            return delegate.entityFactory
        }

    override val entitySpecResolver: EntitySpecResolver
        get() {
            checkInitialized()
            return delegate.entitySpecResolver
        }

    private lateinit var delegate: DomainBuilderContext

    fun init(impl: DomainBuilderContext) {
        if (this::delegate.isInitialized) {
            if (impl != delegate) {
                throw IllegalStateException("already initialized: ${delegate::class.qualifiedName}")
            }
        }

        delegate = impl
    }

    override fun register(entity: EntitySpec<*, Context>) {
        checkInitialized()
        delegate.register(entity)
    }

    override fun register(relation: Relation<*, *>) {
        checkInitialized()
        delegate.register(relation)
    }

    override fun alias(obj: DomainObject, alias: String) {
        checkInitialized()
        delegate.alias(obj, alias)
    }

    override fun build(): Domain = delegate.build()

    private fun checkInitialized() {
        if (!this::delegate.isInitialized) {
            throw IllegalStateException("not initialized")
        }
    }
}
