package de.thecastor.test.szenario.domain

interface Entity : DomainObject {
    val name: String
}