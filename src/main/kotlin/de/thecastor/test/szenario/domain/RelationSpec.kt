package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context

class RelationSpec<TContext : Context, From : Any, To : Any>(
    val from: EntitySpec<From, TContext>,
    val to: EntitySpecProvider<To, TContext>,
    val via: String,
    val using: TContext.() -> Unit = {}
) {
    constructor(
        from: EntitySpec<From, TContext>,
        to: EntitySpec<To, TContext>,
        via: String,
        using: TContext.() -> Unit = {}
    ) : this(from, SimpleProvider(to), via, using)

    override fun toString(): String = "${from.type.qualifiedName} --($via)--> ${to.type.qualifiedName}"

    class Builder<TContext : Context, From : Any, To : Any>(
        private val context: RelationSpecBuilderContext<TContext, From>,
        val to: EntitySpecProvider<To, TContext>,
        private val using: TContext.() -> Unit = {},
        private val onBuild: (Builder<TContext, From, To>, RelationSpec<TContext, From, To>) -> Unit
    ) {
        infix fun via(property: String): RelationSpec<TContext, From, To> =
            RelationSpec(context.subject, to, property, using).also {
                onBuild(this, it)
            }

        override fun toString(): String = "${context.subject.type.qualifiedName} --(?)--> ${to.type.qualifiedName}"
    }

    class RelationSpecBuilderContext<TContext : Context, From : Any>(
        context: TContext,
        val subject: EntitySpec<From, TContext>
    ) : Context by context
}
