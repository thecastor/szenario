package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

interface EntitySpecResolver {
    fun <T : Any> resolve(name: String, type: KClass<T>): EntitySpecProvider<T, Context>
}
