package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

class DefaultDomainBuilder : DomainBuilder {
    private val context: DomainBuilderContext = DomainBuilderContextProxy
    override val entitySpecResolver: EntitySpecResolver by lazy { context.entitySpecResolver }

    init {
        // sets DefaultDomainBuilder as the DomainBuilder implementation
        DefaultDomainBuilderContext.init()
    }

    override fun <TResult : Any> Entity(
        type: KClass<TResult>,
        setUp: EntitySpec<TResult, Context>.() -> Unit
    ): EntitySpec<TResult, Context> =
        EntitySpec<TResult, Context>(context, type)
            .apply { setUp() }
            .also { context.register(it) }
}
