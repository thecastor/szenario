package de.thecastor.test.szenario.domain

import de.thecastor.test.szenario.common.Context
import kotlin.reflect.KClass

interface EntitySpecProvider<T : Any, TContext : Context> {
    val name: String
    val type: KClass<T>
    fun get(): EntitySpec<T, TContext>
}

class SimpleProvider<T : Any, TContext : Context>(
    override val name: String,
    override val type: KClass<T>,
    private val provider: () -> EntitySpec<T, TContext>
) : EntitySpecProvider<T, TContext> {

    constructor(spec: EntitySpec<T, TContext>) : this(spec.name, spec.type, { spec })

    override fun get(): EntitySpec<T, TContext> = provider()
}
