package de.thecastor.test.szenario.common

import de.thecastor.test.szenario.base.EntityFactory

interface Context {
    val parent: Context?
    val entityFactory: EntityFactory

    interface Hierarchical {

    }
}
