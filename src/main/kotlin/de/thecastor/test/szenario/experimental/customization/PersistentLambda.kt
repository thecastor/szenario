package de.thecastor.test.szenario.experimental.customization

import java.io.*

class PersistentLambda<In, Out> private constructor(
    val lambda: (In) -> Out,
    val target: Target<In, Out>
) {

    constructor(lambda: (In) -> Out) : this(lambda, lambda.target)

    fun persist() {
        FileOutputStream(target.fileName).use { fos ->
            ObjectOutputStream(fos).use { oos ->
                oos.writeObject(lambda)
            }
        }
    }

    override fun toString(): String {
        return StringBuilder("Customization(").apply {
            append("\ndomain name=").append(lambda::class.fqdn())
            append("\ndomain path=").append(lambda::class.fullyQualifiedDomainPath())
            append("\nclass=").append(lambda::class.java.declaringClass)
            append("\nname=").append(lambda::class.simpleName)
            append("\nfqdn=").append(lambda::class.qualifiedName)
            append("\npackage=").append(lambda::class.java.packageName)
            append("\npath=").append(lambda::class.getAbsolutePath())
            append("\nexists=").append(lambda::class.getFile().exists())
            append("\ncode=").append(lambda::class.java.protectionDomain.codeSource)
        }.append(")").toString()
    }

    data class Target<In, Out>(val fileName: String) {
        val file: File by lazy {
            File(fileName)
        }
    }

    companion object {

        private val <In, Out> ((In) -> Out).target: Target<In, Out>
            get() = this::class.getAbsolutePath().replace(".class", ".ser").let {
                Target(it)
            }

        fun <In, Out> restore(target: Target<In, Out>): PersistentLambda<In, Out> =
            FileInputStream(target.file).use { fis ->
                ObjectInputStream(fis).use { ois ->
                    ois.readObject()
                }
            }.let {
                @Suppress("UNCHECKED_CAST")
                PersistentLambda(it as (In) -> Out)
            }
    }

}
