package de.thecastor.test.szenario.experimental.customization

import de.thecastor.test.szenario.experimental.customization.pkg.Lambdas

fun main(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    println("Context-free >")
    Lambdas.contextFree.run {
        persist()
        PersistentLambda.restore(target)
    }.also { it.lambda(Unit) }

    println("Contextual >")
    Lambdas.contextual.run {
        persist()
        PersistentLambda.restore(target)
    }.also { it.lambda(Unit) }

    println("Consumer >")
    Lambdas.consumer.run {
        persist()
        PersistentLambda.restore(target)
    }.also { it.lambda("Hello World!") }

    println("Producer >")
    Lambdas.producer.run {
        persist()
        PersistentLambda.restore(target)
    }.also { println("produced '${it.lambda(Unit)}'") }

    println("Function >")
    Lambdas.function.run {
        persist()
        PersistentLambda.restore(target)
    }.also {
        val result = it.lambda("Hello World!")
        println("produced '$result'")
    }
}
