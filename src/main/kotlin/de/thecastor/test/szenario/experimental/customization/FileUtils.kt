package de.thecastor.test.szenario.experimental.customization

import java.io.File
import kotlin.reflect.KClass

fun KClass<*>.fqdn(): String = toString().substringAfter(' ')

fun KClass<*>.fullyQualifiedDomainPath(): String = fqdn().replace('.', File.separatorChar)

fun KClass<*>.getFile(): File =
    File(getAbsolutePath())

fun KClass<*>.getAbsolutePath(): String =
    StringBuilder(java.protectionDomain.codeSource.location.toURI().path)
        .append(fullyQualifiedDomainPath())
        .append(".class")
        .toString()