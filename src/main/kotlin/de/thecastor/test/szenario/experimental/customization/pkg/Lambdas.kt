package de.thecastor.test.szenario.experimental.customization.pkg

import de.thecastor.test.szenario.experimental.customization.PersistentLambda

object Lambdas {

    val contextFree = PersistentLambda<Unit, Unit> {
        println("Hello World!")
    }

    private const val x = "Hello"

    val contextual = PersistentLambda<Unit, Unit> {
        println("$x World!")
    }

    val producer = PersistentLambda<Unit, String> {
        "Hello World!".also {
            println("returning '$it'")
        }
    }

    val consumer = PersistentLambda<String, Unit> {
        println("consumed '$it'")
    }

    val function = PersistentLambda<String, String> {
        println("consumed '$it'")
        "Hello World"
    }

}
