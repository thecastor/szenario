package de.thecastor.test.szenario.build

import com.squareup.kotlinpoet.FileSpec
import java.io.File

private val logger = org.slf4j.LoggerFactory.getLogger("SzenarioGenerate")

fun main(args: Array<String>) {
    require(args.size > 2) {
        "received ${args.size} args, expected 3: package, srcDir, targetDir"
    }

    val basePackage = args[0]
    val srcDir = File(args[1])
    val targetDir = File(args[2])

    logger.info("DSL definition dir: {}", srcDir.absolutePath)
    logger.info("package: {}", basePackage)
    logger.info("DSL generation dir: {}", targetDir.absolutePath)

//    val f: FileSpec = TODO()
}