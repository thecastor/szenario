package de.thecastor.test.szenario.build

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.asTypeName
import de.thecastor.test.szenario.base.spec.DomainEntity
import de.thecastor.test.szenario.domain.Domain
import de.thecastor.test.szenario.domain.DomainBuilderContextProxy
import java.nio.file.Path

fun main(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    de.thecastor.test.szenario.main()

    val basePath = SzenarioGenerator::class.java.protectionDomain.codeSource.location.toURI().path
    println("writing to $basePath...")
    SzenarioGenerator(Path.of(basePath), DomainBuilderContextProxy.build()).build()
}

class SzenarioGenerator(
    private val destination: Path,
    private val domain: Domain
) {

    fun build() {
        generateEntities()

        val className = ClassName("", CLASS_NAME)

        // init block
        val initBlock = CodeBlock.builder()
            .apply {

            }
            .build()

        // Szenario(...) wrapper function
        val contextFun = FunSpec.builder(FUN_NAME)
            .addParameter(
                ParameterSpec.builder(
                    "tests",
                    LambdaTypeName.get(className, emptyList(), Unit::class.asTypeName())
                ).build()
            )
            .build()

        // file
        FileSpec.builder(packageName, FILE_NAME)
            .addType(
                TypeSpec.objectBuilder(className)
                    .addInitializerBlock(initBlock)
                    .addFunction(contextFun)
                    .build()
            )
            .build().writeTo(destination)
    }

    private fun generateEntities() {
        val specPkg = "$packageName.spec"

        domain.entities.forEach {
            val className = it.name.className()
            val actual = it.type

            it.relations.forEach {
                it.value.to.get()
            }

            FileSpec.builder(specPkg, className)
                .addType(
                    TypeSpec.classBuilder(className)
                        .superclass(DomainEntity::class.parameterizedBy(actual))
                        .addSuperclassConstructorParameter("${actual.simpleName}::class")
                        .build()
                )
                .build().writeTo(destination)
        }
    }

    companion object {
        private val packageName = SzenarioGenerator::class.java.packageName
        private const val CLASS_NAME = "Szenario"
        private const val FILE_NAME = CLASS_NAME
        private const val FUN_NAME = "Szenario"

        private fun String.className() = this[0].toUpperCase() + substring(1)
    }
}

interface SzenarioContext<Provider> {

    val provider: Provider
}
