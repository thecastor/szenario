package de.thecastor.test.szenario.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.getByName
import org.gradle.kotlin.dsl.register
import org.gradle.kotlin.dsl.withType
import java.io.File

open class SzenarioPluginExtension {
    var basePackage: String? = null
    var srcDir: String = "src/szenario"
    var targetDir: String = "src/test/generated/java"
}

@Suppress("unused")
class SzenarioPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.tasks.register<GenerateSzenarioTask>(GenerateSzenarioTask.NAME) {
            dependsOn("classes")
        }
        project.tasks.apply {
            withType<Test>().forEach {
                it.dependsOn(GenerateSzenarioTask.NAME)
            }
        }

        val configuration = project.configurations.create("szenario") {
            extendsFrom(
                    project.configurations.getByName("compile"),
                    project.buildscript.configurations.getByName("classpath")
            )
        }
        project.extensions.create<SzenarioPluginExtension>("szenario")

        project.afterEvaluate {
            val srcDir = project.extensions.getByName<SzenarioPluginExtension>("szenario").srcDir
            val sourceSets = project.convention.getPlugin(JavaPluginConvention::class.java).sourceSets

            sourceSets.create("szenario").apply {
                compileClasspath = configuration
                java.srcDirs(srcDir)
            }
        }
    }

}

open class GenerateSzenarioTask : JavaExec() {

    init {
        group = "verification"
        main = "de.thecastor.test.szenario.build.GenerateKt"
        classpath(
                project.buildscript.configurations.getByName("classpath"),
                project.convention.getPlugin(JavaPluginConvention::class.java)
                        .sourceSets.getByName("szenario").compileClasspath
        )

        val configuration = project.extensions.getByName<SzenarioPluginExtension>("szenario")
        configure(configuration)
    }

    private fun configure(config: SzenarioPluginExtension) {
        val inputFiles = project.fileTree(config.srcDir) {
            include("**/*.kt", "**/*.java")
        }

        val outputDir = File(config.targetDir)

        if (!outputDir.exists()) {
            outputDir.mkdirs()
        }

        inputs.files(inputFiles)
        outputs.dir(config.targetDir)
        args = listOf(
            config.basePackage ?: " ",
            config.srcDir,
            config.targetDir
        )
    }

    companion object {
        const val NAME: String = "szenarioGenerate"
    }

}