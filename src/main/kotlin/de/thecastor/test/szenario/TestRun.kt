package de.thecastor.test.szenario

import de.thecastor.test.szenario.domain.Szenario

fun main() {
    Szenario {
        val resolver = entitySpecResolver
        Entity(String::class) {
            name = "something"
            using = {
                entityFactory.create(String::class, this)
            }
            relatesTo(resolver.resolve("blubb", Any::class))
            relatesTo(resolver.resolve("bleh", Int::class))
        }
        Entity(Int::class) {
            name = "bleh"
        }
    }
}
