rootProject.name = "scenario-builder"

pluginManagement {
    val kotlinVersion: String by settings

    plugins {
        kotlin("jvm") version kotlinVersion
    }

    extra["kotlinVersion"] = kotlinVersion
}