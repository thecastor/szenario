import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

buildscript {
    apply("version.gradle.kts")
}

plugins {
    kotlin("jvm")
    `java-gradle-plugin`
    `kotlin-dsl`
    `java-library`
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

group = "de.thecastor.test"
version = "0.1"

gradlePlugin {
    plugins {
        create("SzenarioPlugin") {
            id = "de.thecastor.szenario"
            implementationClass = "de.thecastor.test.szenario.gradle.SzenarioPlugin"
        }
    }
}

repositories {
    mavenCentral()
}

val kotlinVersion: String by project
val kotlinPoetVersion: String by project
val junitVersion: String by project
val hamcrestVersion: String by project
val kotlinLoggingVersion: String by project
val slf4jVersion: String by project

dependencies {
    implementation(kotlin("reflect", kotlinVersion))
    implementation("com.squareup", "kotlinpoet", kotlinPoetVersion)
    implementation("org.slf4j", "slf4j-api", slf4jVersion)

    testImplementation(gradleTestKit())
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
    testImplementation("org.jetbrains.kotlin", "kotlin-test", kotlinVersion)
    testImplementation("org.jetbrains.kotlin", "kotlin-test-junit", kotlinVersion)
    testImplementation("org.hamcrest", "hamcrest-library", hamcrestVersion)
}

sourceSets.main {
    java.srcDirs("src/main/kotlin")
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "17"
            freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlin.RequiresOptIn"
        }
    }
    withType<JavaCompile> {
        sourceCompatibility = JavaVersion.VERSION_17.toString()
        targetCompatibility = JavaVersion.VERSION_17.toString()
    }
    withType<Test> {
        useJUnitPlatform()
    }
    register<ShadowJar>("pluginJar") {
        group = "szenario"
        archiveVersion.set("${project.version}")
        archiveBaseName.set("szenario-plugin")
        manifest {
            attributes["Main-Class"] = "de.thecastor.test.szenario.build.GenerateKt"
        }
        from(sourceSets.main.get().output)
        exclude("de/thecastor/test/szenario/base")
        doLast {
            if (File("$projectDir/plugin-test").isDirectory) {
                copy {
                    from("${project.buildDir}/libs/$archiveFileName")
                    into("$projectDir/plugin-test/libs/szenario.jar")
                }
            }
        }
    }
    register<ShadowJar>("baseFileJar") {
        group = "szenario"
        archiveVersion.set("${project.version}")
        archiveBaseName.set("szenario-base")
        manifest {
        }
        from(sourceSets.main.get().output)
        include("de/thecastor/test/szenario/common/**", "de/thecastor/test/szenario/base/**")
        relocate("de.thecastor.test.szenario.base", "de.thecastor.test.szenario")
    }
}
